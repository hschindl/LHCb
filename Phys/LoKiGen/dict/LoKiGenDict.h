// $Id$
// ============================================================================
#ifndef LOKI_LOKIGENDICT_H
#define LOKI_LOKIGENDICT_H 1
// ============================================================================
// Include files
// ============================================================================
#include "LoKi/LoKiGen_dct.h"
// ============================================================================
/** @file
 *  The dictionaries for the package Phys/LoKiGen
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2007-12-01
 */
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_LOKIGENDICT_H
// ============================================================================
