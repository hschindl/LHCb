
//================================================================
/** @file StlArray.h
 *
 *  Header file for std::array + Additional Gaudi methods
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   2015-02-07
 */
//================================================================

#ifndef RICHUTILS_STLARRAY_H 
#define RICHUTILS_STLARRAY_H 1

// STL
#include <array>

// Gaudi
#include "GaudiKernel/ToStream.h"

#endif // RICHUTILS_STLARRAY_H
