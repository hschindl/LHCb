
//-----------------------------------------------------------------------------
/** @file RichZeroSuppALICEData.h
 *
 *  Header file for RICH Alice mode ZS data formats
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   2003-11-07
 */
//-----------------------------------------------------------------------------

#pragma once

#include "RichDAQKernel/RichZeroSuppALICEData_V1.h"
