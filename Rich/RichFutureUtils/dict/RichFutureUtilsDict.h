#ifndef DICT_RICHUTILSDICT_H 
#define DICT_RICHUTILSDICT_H 1

#include "RichFutureUtils/RichHypoData.h"
#include "RichFutureUtils/RichGeomPhoton.h"

// instanciate templated classes
namespace 
{
  struct _Instantiations 
  {
    Rich::Future::HypoData<float>         obj_1;
    Rich::Future::HypoData<double>        obj_2;
  };
}

#endif // DICT_RICHUTILSDICT_H
