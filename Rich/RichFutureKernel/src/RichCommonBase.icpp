
//-----------------------------------------------------------------------------
/** @file RichCommonBase.icpp
 *
 *  Implementation file for RICH base class : Rich::CommonBase
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2005-08-27
 */
//-----------------------------------------------------------------------------

#ifndef RICHFUTUREKERNEL_RICHCOMMONBASE_ICPP
#define RICHFUTUREKERNEL_RICHCOMMONBASE_ICPP 1

// STL
#include <sstream>
#include <string>
#include <algorithm>

// local
#include "RichFutureKernel/RichCommonBase.h"

// Gaudi
#include "GaudiKernel/IJobOptionsSvc.h"

// Disable warning on windows
#ifdef _WIN32
#pragma warning ( disable:4661 ) // incomplete explicit templates
#endif

//=============================================================================
// Constructor initialisation
//=============================================================================
template <class PBASE>
void Rich::Future::CommonBase<PBASE>::initRichCommonConstructor()
{
  // nothing yet ..
}

//=============================================================================
// Initialisation
//=============================================================================
template <class PBASE>
StatusCode Rich::Future::CommonBase<PBASE>::initialize()
{
  // Execute the base class initialize
  const StatusCode sc = PBASE::initialize();
  if ( sc.isFailure() )
  { return this -> Error( "Failed to initialise Gaudi Base class", sc ); }

  // Load some common tools by default
  m_jos = this -> template svc<IJobOptionsSvc>( "JobOptionsSvc" );

  // Printout from initialize
  if ( this -> msgLevel(MSG::DEBUG) )
    this -> debug() << "Initialize" << endmsg;

  return sc;
}
//=============================================================================

//=============================================================================
// Finalisation
//=============================================================================
template <class PBASE>
StatusCode Rich::Future::CommonBase<PBASE>::finalize()
{
  // Printout from finalization
  if ( this -> msgLevel(MSG::DEBUG) )
    this -> debug() << "Finalize" << endmsg;

  // Finalise base class and return
  return PBASE::finalize();
}
//=============================================================================

#endif // RICHFUTUREKERNEL_RICHCOMMONBASE_ICPP
