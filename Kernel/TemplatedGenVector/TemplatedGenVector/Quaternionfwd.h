// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

#ifndef LHCbROOT_Math_GenVector_Quaternionfwd
#define LHCbROOT_Math_GenVector_Quaternionfwd  1


namespace LHCbROOT {

  namespace Math {

  /**
      Class describing a rotation represented by a quaternion
   */

class Quaternion;

}  // namespace Math
}  // namespace LHCbROOT

#endif  // LHCbROOT_Math_GenVector_Quaternionfwd
