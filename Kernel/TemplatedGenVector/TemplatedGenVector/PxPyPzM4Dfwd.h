// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

#ifndef LHCbROOT_Math_GenVector_PxPyPzM4Dfwd
#define LHCbROOT_Math_GenVector_PxPyPzM4Dfwd  1


namespace LHCbROOT {

  namespace Math {

template <class Scalar>
class PxPyPzM4D;

  } // end namespace Math

} // end namespace LHCbROOT


#endif /* LHCbROOT_Math_GenVector_PxPyPzM4Dfwd  */
