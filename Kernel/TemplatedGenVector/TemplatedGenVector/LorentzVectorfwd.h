// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

 /**********************************************************************
  *                                                                    *
  * Copyright (c) 2005 , LCG LHCbROOT MathLib Team                         *
  *                                                                    *
  *                                                                    *
  **********************************************************************/

// Header file for class LorentzVectorfwd
//
// Created by: moneta  at Tue May 31 21:06:43 2005
//
// Last update: Tue May 31 21:06:43 2005
//
#ifndef LHCbROOT_Math_GenVector_LorentzVectorfwd
#define LHCbROOT_Math_GenVector_LorentzVectorfwd  1


namespace LHCbROOT {

  namespace Math {


    // forward declaretions of Lorentz Vectors and type defs definitions

    template<class CoordSystem> class LorentzVector;


  } // end namespace Math

} // end namespace LHCbROOT

#endif

