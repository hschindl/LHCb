// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

#ifndef LHCbROOT_Math_GenVector_Cylindrical3Dfwd
#define LHCbROOT_Math_GenVector_Cylindrical3Dfwd  1


namespace LHCbROOT {

  namespace Math {

  /**
      Class describing a 3D Cylindrical Eta coordinate system
      (rho, z, phi coordinates)
   */

template <class T>
class Cylindrical3D;

}
}

#endif
