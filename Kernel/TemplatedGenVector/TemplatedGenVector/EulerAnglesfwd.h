// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

#ifndef LHCbROOT_Math_GenVector_EulerAnglesfwd
#define LHCbROOT_Math_GenVector_EulerAnglesfwd  1


namespace LHCbROOT {

  namespace Math {

  /**
      Class describing a rotation represented
      by Euler angles phi, theta, psi
   */

class EulerAngles;

}  // namespace Math
}  // namespace LHCbROOT

#endif  // LHCbROOT_Math_GenVector_EulerAnglesfwd
