// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

#ifndef LHCbROOT_Math_GenVector_BoostXfwd
#define LHCbROOT_Math_GenVector_BoostXfwd  1


namespace LHCbROOT {

  namespace Math {

  /**
      Class describing a pure Lorentz Boost along the X axis
   */

class BoostX;

}  // namespace Math
}  // namespace LHCbROOT

#endif  // LHCbROOT_Math_GenVector_BoostXfwd
