// @(#)root/mathcore:$Id$
// Authors: M. Fischler   2005

#ifndef LHCbROOT_Math_GenVector_Boostfwd
#define LHCbROOT_Math_GenVector_Boostfwd  1


namespace LHCbROOT {

  namespace Math {

  /**
      Class describing a pure boost in some direction
   */

class Boost;

}  // namespace Math
}  // namespace LHCbROOT

#endif  // LHCbROOT_Math_GenVector_Boostfwd
