// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

#ifndef LHCbROOT_Math_GenVector_AxisAnglefwd
#define LHCbROOT_Math_GenVector_AxisAnglefwd  1


namespace LHCbROOT {

  namespace Math {

  /**
      Class describing a rotation represented
      by an axis and an angle of rotation
   */

class AxisAngle;

}  // namespace Math
}  // namespace LHCbROOT

#endif  // LHCbROOT_Math_GenVector_AxisAnglefwd
