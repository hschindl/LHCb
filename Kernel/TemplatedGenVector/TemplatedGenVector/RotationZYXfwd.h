// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

#ifndef LHCbROOT_Math_GenVector_RotationZYXfwd
#define LHCbROOT_Math_GenVector_RotationZYXfwd  1


namespace LHCbROOT {

  namespace Math {

  /**
      Class describing a rotation represented by a 3-2-1 (Z-Y-X) Euler angles
   */

class RotationZYX;

}  // namespace Math
}  // namespace LHCbROOT

#endif  // LHCbROOT_Math_GenVector_Rotation3Dfwd
