// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

#ifndef LHCbROOT_Math_GenVector_RotationZfwd
#define LHCbROOT_Math_GenVector_RotationZfwd  1


namespace LHCbROOT {

  namespace Math {

  /**
      Class describing a rotation about the Z axis
   */

class RotationZ;

}  // namespace Math
}  // namespace LHCbROOT

#endif  // LHCbROOT_Math_GenVector_RotationZfwd
