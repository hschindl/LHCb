// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

#ifndef LHCbROOT_Math_GenVector_BoostYfwd
#define LHCbROOT_Math_GenVector_BoostYfwd  1


namespace LHCbROOT {

  namespace Math {

  /**
      Class describing a pure Lorentz Boost along the Y axis
   */

class BoostY;

}  // namespace Math
}  // namespace LHCbROOT

#endif  // LHCbROOT_Math_GenVector_BoostYfwd
