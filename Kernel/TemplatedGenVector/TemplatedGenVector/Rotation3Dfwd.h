// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

#ifndef LHCbROOT_Math_GenVector_Rotation3Dfwd
#define LHCbROOT_Math_GenVector_Rotation3Dfwd  1


namespace LHCbROOT {

  namespace Math {

  /**
      Class describing a rotation represented by a 3x3 matrix
   */

class Rotation3D;

}  // namespace Math
}  // namespace LHCbROOT

#endif  // LHCbROOT_Math_GenVector_Rotation3Dfwd
