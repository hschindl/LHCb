// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

#ifndef LHCbROOT_Math_GenVector_CylindricalEta3Dfwd
#define LHCbROOT_Math_GenVector_CylindricalEta3Dfwd  1


namespace LHCbROOT {

  namespace Math {

  /**
      Class describing a 3D Cylindrical Eta coordinate system
      (rho, eta, phi coordinates)
   */

template <class T>
class CylindricalEta3D;

}
}

#endif
