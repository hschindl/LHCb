// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

#ifndef LHCbROOT_Math_GenVector_DisplacementVector3Dfwd
#define LHCbROOT_Math_GenVector_DisplacementVector3Dfwd  1


namespace LHCbROOT {
  namespace Math {

    /**
        Class template describing a 3D displacement vector
     */
    template< class CoordSystem, class Tag  >
    class DisplacementVector3D;

  }  // namespace Math
}  // namespace LHCbROOT


#endif
