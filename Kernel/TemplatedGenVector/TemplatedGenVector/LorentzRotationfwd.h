// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

 /**********************************************************************
  *                                                                    *
  * Copyright (c) 2005   LHCbROOT CERN / FNAL MATHLIB Group              *
  *                                                                    *
  *                                                                    *
  **********************************************************************/

// Header file defining forward declarations for LorentzVector classes
//
// Created by: Lorenzo Moneta    at Thu May 12 11:19:02 2005
// Major revamp by: M. Fischler  at Tue Aug  9  2005
//
// Last update: Tue Aug  9  2005
//
#ifndef LHCbROOT_Math_GenVector_LorentzRotationfwd
#define LHCbROOT_Math_GenVector_LorentzRotationfwd  1


namespace LHCbROOT {
  namespace Math {
    class LoretzRotation;
  } // end namespace Math
} // end namespace LHCbROOT


#endif /* LHCbROOT_Math_GenVector_LorentzRotationfwd  */
