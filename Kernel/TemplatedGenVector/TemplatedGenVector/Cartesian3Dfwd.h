// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

#ifndef LHCbROOT_Math_GenVector_Cartesian3Dfwd
#define LHCbROOT_Math_GenVector_Cartesian3Dfwd  1


namespace LHCbROOT {

  namespace Math {

template <class T>
class Cartesian3D;

  } // end namespace Math

} // end namespace LHCbROOT


#endif /* LHCbROOT_Math_GenVector_Cartesian3Dfwd  */
