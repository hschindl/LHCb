// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

#ifndef LHCbROOT_Math_GenVector_PositionVector3Dfwd
#define LHCbROOT_Math_GenVector_PositionVector3Dfwd  1


namespace LHCbROOT {

  namespace Math {

     /**
         Class describing a 3D Position vector
     */

     template <class CoordSystem, class Tag>
     class PositionVector3D;

  }
}

#endif
