// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

#ifndef LHCbROOT_Math_GenVector_Polar2Dfwd
#define LHCbROOT_Math_GenVector_Polar2Dfwd  1


namespace LHCbROOT {

   namespace Math {

      /**
          Class describing a 2D Polar coordinate system
      (r, phi coordinates)
      */

      template <class T>
      class Polar2D;

   }
}

#endif
