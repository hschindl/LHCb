// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

#ifndef LHCbROOT_Math_GenVector_RotationYfwd
#define LHCbROOT_Math_GenVector_RotationYfwd  1


namespace LHCbROOT {

  namespace Math {

  /**
      Class describing a rotation about the Y axis
   */

class RotationY;

}  // namespace Math
}  // namespace LHCbROOT

#endif  // LHCbROOT_Math_GenVector_RotationYfwd
