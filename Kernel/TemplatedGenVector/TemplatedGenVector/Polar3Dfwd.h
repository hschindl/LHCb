// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

#ifndef LHCbROOT_Math_GenVector_Polar3Dfwd
#define LHCbROOT_Math_GenVector_Polar3Dfwd  1


namespace LHCbROOT {

  namespace Math {

  /**
      Class describing a 3D Polar coordinate system
      (r, theta, phi coordinates)
   */

template <class T>
class Polar3D;

}
}

#endif
