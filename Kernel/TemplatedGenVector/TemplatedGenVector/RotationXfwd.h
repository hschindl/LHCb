// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

#ifndef LHCbROOT_Math_GenVector_RotationXfwd
#define LHCbROOT_Math_GenVector_RotationXfwd  1


namespace LHCbROOT {

  namespace Math {

  /**
      Class describing a rotation about the X axis
   */

class RotationX;

}  // namespace Math
}  // namespace LHCbROOT

#endif  // LHCbROOT_Math_GenVector_RotationXfwd
