// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

#ifndef LHCbROOT_Math_GenVector_PtEtaPhiE4Dfwd
#define LHCbROOT_Math_GenVector_PtEtaPhiE4Dfwd  1


namespace LHCbROOT {

  namespace Math {

template <class Scalar>
class PtEtaPhiE4D;

  } // end namespace Math

} // end namespace LHCbROOT


#endif /* LHCbROOT_Math_GenVector_PtEtaPhiE4Dfwd  */
