// @(#)root/mathcore:$Id$
// Authors: W. Brown, M. Fischler, L. Moneta    2005

#ifndef LHCbROOT_Math_GenVector_BoostZfwd
#define LHCbROOT_Math_GenVector_BoostZfwd  1


namespace LHCbROOT {

  namespace Math {

  /**
      Class describing a pure Lorentz Boost along the Z axis
   */

class BoostZ;

}  // namespace Math
}  // namespace LHCbROOT

#endif  // LHCbROOT_Math_GenVector_BoostZfwd
