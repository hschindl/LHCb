#ifndef COMPAREPROTOPARTICLE_H
#define COMPAREPROTOPARTICLE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

#include "Event/ProtoParticle.h"
#include "Event/PackedProtoParticle.h"

/** @class CompareProtoParticle CompareProtoParticle.h
 *  Compare two containers of ProtoParticles
 *
 *  @author Olivier Callot
 *  @date   2008-11-14
 */
class CompareProtoParticle : public GaudiAlgorithm
{

public:

  /// Standard constructor
  CompareProtoParticle( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution

private:

  std::string m_inputName;
  std::string m_testName;

};

#endif // COMPAREPROTOPARTICLE_H
