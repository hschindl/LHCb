#ifndef     __DETDESC_SOLID_SOLIDS_H__
#define     __DETDESC_SOLID_SOLIDS_H__


//
// primitive solids
#include "DetDesc/SolidPrimitives.h" 

//
// advanced solids 
#include "DetDesc/SolidAdvanced.h" 



#endif  //  __DETDESC_SOLID_SOLIDS_H__

